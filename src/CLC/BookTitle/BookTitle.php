<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class BookTitle extends DB{
    private $id;
    private $book_name;
    private $author_name;
     
    public function  setData($postData){
    	//var_dump($postData);
        if(array_key_exists('id',$postData)){$this->id = $postData['id'];}
        if(array_key_exists('bookName',$postData)){$this->book_name = $postData['bookName'];}
        if(array_key_exists('AuthorName',$postData)){$this->author_name = $postData['AuthorName'];}
        
    }

    public function store(){
        $arrData = array($this->book_name,$this->author_name);
        //var_dump($arrData);
        $sql = "INSERT into book(book_name,author_name) VALUES (?,?)";
        //var_dump($this->DBH);die();
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
    }

    public function index(){
        $sql = "select * from book where soft_delete='No'";
        
        $STH=$this->DBH->query($sql);
        

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }

    public function view(){
        $sql = "select * from book where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }

    public function update(){
        $arrData = array($this->book_name,$this->author_name);
        //var_dump($arrData);
        $sql = "UPDATE book SET book_name=?,author_name=? WHERE id=".$this->id;
        //var_dump($this->DBH);die();
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if($result)
            message::message("success!");
        else("failed!");
        utility::redirect("index.php");
    }

    public function delete(){

        $sql = "DELETE from book  WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);

        if($result)
            Message::message("success!");
        else("failed!");
        Message::message("failed!");
        utility::redirect("index.php");
    }
}
?>